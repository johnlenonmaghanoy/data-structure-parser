/*const getFruit = async name => {
	const fruits = {
		"pineaple": "eyes"
	};

	return fruits[ name ];
}

const a = await getFruit( "pineaple" );

async function getFruit( userName ){
	var response = await fetch( `https://api.github.com/users/${userName}` );
	let data = await response.json( )
	return data;
}

var testA = await getFruit( "johnlenonmaghanoy" );
console.log( "Sample jlm", testA );*/


const fetch = require( "node-fetch" );

async function showGitHubUser( userName ){
	var response = await fetch( `https://api.github.com/users/${userName}` );
	return await response.json( );
}

showGitHubUser( "johnlenonmaghanoy" )
.then( user => {
	console.log( "Login", user.login );
	console.log( "Followers", user.followers );
} );
