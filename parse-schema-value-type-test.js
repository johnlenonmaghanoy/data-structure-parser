const async = require( "async" );
const parseSchemaValueType = require( "./parse-schema-value-type.js" );

let valueTypeCopy = undefined;

async.waterfall(
	[
		function testParseSchemaValueType( callback ){
			parseSchemaValueType(
				"[@type:string,jlm]",
				function( trigger, valueType, optionData ){
					if(
						trigger === true
					){
						callback( true );
					}
					else if(
							(
								trigger
								instanceof Error
							)
						===	true
					){
						callback( trigger );
					}
					else if(
						typeof valueType !== "undefined"
					){
						valueTypeCopy = valueType

						callback( );
					}
				}
			);
		}
	],
	function lastly( trigger ){
		if(
			trigger === true
		){

		}
		else if(
			(
				trigger
				instanceof Error
			)
			=== true
		){
			console.log( "Bug", trigger );
		}
		else{
			console.log( "Value type", valueTypeCopy );
		}
	}
);
