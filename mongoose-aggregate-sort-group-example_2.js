SERVICE
.get(
	"/service/interface/transport-booking/schedule/:scheduleShortCode/disabled-date-setting/list",
	function( request, response ){
		const scheduleShortCode = (
			request.params.scheduleShortCode
		);

		let pageIndex = (
			parseInt(
				request.query.page
			)
		);

		let getScheduleDisabledDateListQuery = { };

		let scheduleDisabledDateList = [ ];

		const TRANSPORT_BOOKING_SCHEDULE_DISABLED_DATE = mongoose.model( "TransportBookingScheduleDisabledDate" );

		let pageData = { };

		waterfall(
			[
				function prepareDisableDateListQueryParameter( callback ){
					getScheduleDisabledDateListQuery.scheduleShortCode = scheduleShortCode;

					getScheduleDisabledDateListQuery.targetServiceType = {
						$in: [
							"general",
							"online",
							"walk-in"
						]
					};

					getScheduleDisabledDateListQuery.$and = (
							getScheduleDisabledDateListQuery.$and
						||	[ ]
					);

					getScheduleDisabledDateListQuery.$and
					.push(
						{
							$or: [
								{
									"targetApplyType": {
										$in: [
											"once",
											"repeat"
										]
									}
								}
							]
						}
					);

					callback( );
				},

				function getScheduleDisabledDate( callback ){
					TRANSPORT_BOOKING_SCHEDULE_DISABLED_DATE
					.aggregate(
						[
							{
								"$match": getScheduleDisabledDateListQuery
							},
							{
								"$sort": {
									"targetYear": -1,
									"targetMonth": -1,
									"targetDay": -1
								}
							},
							{
								"$group": {
									"_id": "$targetApplyType",
									"documentList": {
										"$addToSet": "$$ROOT"
									}
								}
							}
						],
						function( error, result ){
							console.log( "Error", error );
							console.log( "Document list", result[ 0 ].documentList );
							callback( true );
						}
					);
				}
			],
			function lastly( error, status ){
				if(
					error === true
				){
					console.log(
						"cannot get transport booking schedule disabled date list",
						"cannot determine status"
					);

					response
					.status( 200 )
					.json(
						{
							"responseState": "aborted",
							"responseCode": 200,

							"procedureStatus": false,
							"requestStatus": false,

							"responseData": [ ],

							"responseMessage": [
								"cannot request get transport booking schedule disabled date list",
								"request get transport booking schedule disabled date list aborted"
							]
						}
					);
				}
				else if(
					error instanceof Error
				){
					console.log(
						"cannot get schedule disable date list",
						"error:", util.inspect( error )
					);

					response
					.status( 200 )
					.json(
						{
							"responseState": "error",
							"responseCode": 200,

							"procedureStatus": false,
							"requestStatus": false,

							"responseData": [ ],

							"responseMessage": [
								"cannot request get transport booking schedule disabled date list",
								"request get transport booking schedule disabled date list failed"
							]
						}
					);
				}
				else{
					response
					.status( 200 )
					.json(
						{
							"responseState": "success",
							"responseCode": 200,

							"procedureStatus": true,
							"requestStatus": true,

							"responseData": scheduleDisabledDateList,
							"pageData": pageData
						}
					);
				}
			}
		);
	}
);
