"use strict";

const STRING_TYPE_PATTERN = /^\[@type\:+string+\,+[a-z][\-a-z0-9]+\]/;

const parseSchemaValueType = function parseSchemaValueType( option, callback ){
	let value = option;

	if(
			(
				typeof value == "undefined"
			)
		||	(
					typeof value == "string"
				&&	value.length == 0
			)
		||	(
					typeof value == "number"
				&&	isNaN( value ) == true
			)
		||	(
					typeof value == "object"
				&&	(
							value === null
						||
							Object.keys( value ).length <= 0
					)
			)
	){
		if(
				typeof
				callback
			==	"function"
		){
			var error = (
				new	Error(
					"invalid value"
				)
			);

			callback(
				error,
				undefined,
				option
			);
		}

		return;
	}

	if(
			typeof value == "string"
		&&	value.length > 0
		&&	(
				STRING_TYPE_PATTERN.test( value )
			)
			== false
	){
		if(
				typeof
				callback
			==	"function"
		){
			callback(
				undefined,
				[
					(
						"@type:@valueType"
					)
					.replace(
						"@valueType",
						typeof value
					),
					value
				],
				option
			);
		}
	}

	if(
			typeof value == "string"
		&&	value.length > 0
		&&	(
				STRING_TYPE_PATTERN.test( value )
			)
			== true
	){
		let valueList = (
			value
			.replace( "[", "" )
			.replace( "]", "" )
			.split( "," )
		);

		if(
				Array.isArray( valueList )
			&&	valueList.length == 2
		){
			if(
					typeof
					callback
				==	"function"
			){
				callback(
					undefined,
					[
						(
							"@type:@valueType"
						)
						.replace(
							"@valueType",
							typeof valueList[ 1 ]
						),
						valueList[ 1 ]
					],
					option
				);
			}
		}
	}

	if(
		typeof value == "boolean"
	){
		if(
				typeof
				callback
			==	"function"
		){
			callback(
				undefined,
				[
					(
						"@type:@valueType"
					)
					.replace(
						"@valueType",
						typeof value
					),
					value
				],
				option
			);
		}
	}

	if(
			typeof value == "number"
		&&	(
				isNaN( value )
			)
			== false
	){
		if(
				typeof
				callback
			==	"function"
		){
			callback(
				undefined,
				[
					(
						"@type:@valueType"
					)
					.replace(
						"@valueType",
						typeof value
					),
					value
				],
				option
			);
		}
	}

	if(
			typeof value == "object"
		&&	Object.keys( value ).length > 0
	){
		if(
				typeof
				callback
			==	"function"
		){
			callback(
				undefined,
				[
					(
						"@type:@valueType"
					)
					.replace(
						"@valueType",
						typeof value
					),
					value
				],
				option
			);
		}
	}
};

module.exports = parseSchemaValueType;
