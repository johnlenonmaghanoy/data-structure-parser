{
	"$project": {
		"_id": "$_id",

		"positiveCredit": {
			$cond: [
				{
					$eq: [
						"$polarity",
						"positive"
					]
				},
				"$amount",
				0
			]
		},

		"negativeCredit": {
			$cond: [
				{
					$eq: [
						"$polarity",
						"negative"
					]
				},
				"$amount",
				0
			]
		}
	}
},
{
	"$group": {
		"_id": 0,

		"totalPositiveCredit": {
			"$sum": "$positiveCredit"
		},

		"totalNegativeCredit": {
			"$sum": "$negativeCredit"
		}
	}
}

{
	$group: {
		"_id": {
			"_departureDate": "$_departureDate"
		},
		"_departureDate": {
			$first: "$_departureDate"
		},
		"__departureDate": {
			$first: "$__departureDate"
		}
	}
},

{
	"$group": {
		"_id": 0,

		"totalPositiveCredit": {
			"$sum": "$positiveCredit"
		},

		"totalNegativeCredit": {
			"$sum": "$negativeCredit"
		}
	}
}

{
	$group: {
		"_id": {
			"province": "$travelPoints.originProvince",
			"area": "$travelPoints.originArea",
			"point": "$travelPoints.originName",
			"countryName": "$travelPoints.originCountryName",
			"countryCode": "$travelPoints.originCountryCode",
			"timeZone": "$travelPoints.originTimeZone"
		}
	}
}

{
	$group: {
		"_id": null,
		"total": {
			$sum: "$count"
		}
	}
}

{
	$group: {
		"_id": null,
		"claimed": {
			$sum: "$count"
		}
	}
}


{ "$group": {
	_id: {
		"etd": "$value.source.BUNKS.etd",
		"vesselCode": "$value.source.BUNKS.vesselCode",
		"vesselNumber": "$value.source.BUNKS.vesselNumber",
		"vesselYear": "$value.source.BUNKS.vesselYear",
		"colorCode": "$value.source.BUNKS.colorCode",
		"alpha": "$value.source.BUNKS.alpha",
		"clusterNumber": "$value.source.BUNKS.clusterNumber",
		"tier": "$value.source.BUNKS.tier",
		"accomodation": "$value.source.BUNKS.accomodation",
		"origin": "$value.source.BUNKS.origin",
		"destination": "$value.source.BUNKS.destination"
	},
	count: { "$sum": 1 }
} }
